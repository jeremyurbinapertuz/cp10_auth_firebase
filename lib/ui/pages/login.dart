import 'package:flutter/material.dart';
import 'package:flutter_auth1270_firebase/controller/controllerauth.dart';
import 'package:flutter_auth1270_firebase/ui/pages/chat/chat.dart';
import 'package:flutter_auth1270_firebase/ui/pages/estados/listarestados.dart';
import 'package:flutter_auth1270_firebase/ui/pages/inicio.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController usuario = TextEditingController();
  TextEditingController passwd = TextEditingController();

  Controllerauth controluser = Get.find();

  @override
  void initState() {
    super.initState();
    autoLogIn();
  }

  void autoLogIn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? _email = prefs.getString('email');
    final String? _passw = prefs.getString('clave');
    print(_email);


    if (_email != null) {
      setState(() {
        usuario.text = _email;
        passwd.text = _passw!;
        // _login(usuario.text, passwd.text);
      });
      return;
    }
  }

  _login(theEmail, thePassword) async {
    print('_login $theEmail $thePassword');
    try {
      await controluser.ingresarEmail(theEmail, thePassword);
      // Get.to(() => ListaMensajes());
      Get.to(() => ListaMensajeros2(title: 'Lista de Estados'));
    } catch (err) {
      print(err.toString());
      Get.snackbar(
        "Login",
        err.toString(),
        icon: Icon(Icons.person, color: Colors.red),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }

  _registro(theEmail, thePassword) async {
    print('_login $theEmail $thePassword');
    try {
      await controluser.registrarEmail(theEmail, thePassword);
      // Get.to(() => Inicio());
      Get.to(() => ListaMensajeros2(title: 'Lista de Estados'));
    } catch (err) {
      print(err.toString());
      Get.snackbar(
        "Login",
        err.toString(),
        icon: Icon(Icons.person, color: Colors.red),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }

  _google_ing() async {
    try {
      await controluser.ingresarGoogle();
      // Get.to(() => ListaMensajes());
      Get.to(() => ListaMensajeros2(title: 'Lista de Estados'));
    } catch (err) {
      print(err.toString());
      Get.snackbar(
        "Login",
        err.toString(),
        icon: Icon(Icons.person, color: Colors.red),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(50.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 80.0,
                backgroundImage: NetworkImage(
                    'https://us.123rf.com/450wm/rashadashurov/rashadashurov1911/rashadashurov191101490/133749415-concepto-de-cuenta-icono-de-l%C3%ADnea-azul-elemento-delgado-simple-sobre-fondo-oscuro-dise%C3%B1o-de-s%C3%ADmbolo-.jpg?ver=6'),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                key: const Key('user'),
                controller: usuario,
                decoration: InputDecoration(hintText: 'Ingrese el Correo'),
              ),
              TextField(
                key: const Key('pass'),
                controller: passwd,
                obscureText: true,
                decoration: InputDecoration(hintText: 'Ingrese la Contraseña'),
              ),
              // Text('Testing demo', style: Theme.of(context).textTheme.headline4),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                      onPressed: () {
                        _login(usuario.text, passwd.text);
                      },
                      icon: Icon(Icons.login_sharp)),
                  IconButton(
                      onPressed: () {
                        _registro(usuario.text, passwd.text);
                      },
                      icon: Icon(Icons.app_registration_sharp))
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Obx(() => Text(controluser.userf)),
              // Text('Testing Demo'),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            _google_ing();
          },
          child: FaIcon(
            FontAwesomeIcons.google,
            color: Colors.white,
          )),

    );
  }
}
