import 'package:flutter/material.dart';
import 'package:flutter_auth1270_firebase/controller/controllerauth.dart';
import 'package:flutter_auth1270_firebase/controller/controlrealtime.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

class Inicio extends StatefulWidget {
  const Inicio({Key? key}) : super(key: key);

  @override
  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  TextEditingController docid = TextEditingController();
  TextEditingController nombre = TextEditingController();
  TextEditingController apellido = TextEditingController();
  TextEditingController direccion = TextEditingController();
  TextEditingController telefono = TextEditingController();

  Controllerauth controluser = Get.find();
  ControllerRealTime controlrt = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(controluser.userf),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Column(
          children: [
            TextField(
              controller: docid,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(hintText: 'Ingrese el documento'),
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              controller: nombre,
              decoration: InputDecoration(hintText: 'Ingrese el nombre'),
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              controller: apellido,
              decoration: InputDecoration(hintText: 'Ingrese el apellido'),
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              controller: direccion,
              decoration: InputDecoration(hintText: 'Ingrese la direccion'),
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              controller: telefono,
              decoration: InputDecoration(hintText: 'Ingrese el telefono'),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          controlrt.createData(docid.text, nombre.text, apellido.text, direccion.text, telefono.text, controluser.uid);
          docid.clear();
          nombre.clear();
          apellido.clear();
          direccion.clear();
          telefono.clear();
        },
        child: FaIcon(
          FontAwesomeIcons.plus,
          color: Colors.white,
        ),
      ),
    );
  }
}
