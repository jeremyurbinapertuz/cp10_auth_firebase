import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_auth1270_firebase/domain/models/mensajes.dart';

import 'package:get/get.dart';

class Controlchat extends GetxController {
  final DatabaseReference _mensajesRef =
  FirebaseDatabase.instance.reference().child('mensajes');

  void guardarMensaje(Mensaje mensaje) {
    _mensajesRef.push().set(mensaje.toJson());
  }

  void deleteMensaje(String idmensaje) {
    _mensajesRef.child(idmensaje).remove();
  }

  Query getMensajes() => _mensajesRef;
}
