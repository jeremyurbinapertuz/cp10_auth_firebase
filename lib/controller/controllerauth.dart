import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Controllerauth extends GetxController {
  final FirebaseAuth auth = FirebaseAuth.instance;
  late Rx<dynamic> _usuarior = "Sin Registro".obs;
  late Rx<dynamic> _uid = "".obs;
  late Rx<dynamic> _name = "".obs;
  late Rx<dynamic> _photo = "".obs;

  String get userf => _usuarior.value;
  String get uid => _uid.value;
  String get name => _name.value;
  String get photo => _photo.value;

  Future<void> registrarEmail(dynamic _email, dynamic _passw) async {
    try {
      UserCredential usuario = await auth.createUserWithEmailAndPassword(
          email: _email, password: _passw);

      _usuarior.value = usuario.user!.email;
      _uid.value = usuario.user!.uid;

      _name.value = usuario.user!.email;
      _photo.value = 'https://static.wikia.nocookie.net/shingeki-no-kyojin/images/2/2d/Levi_Ackerman_%28Anime%29.png/revision/latest?cb=20180816051528&path-prefix=es';

      print(usuario);
      await guardarusuario(_usuarior.value, _passw);

      return Future.value(true);
      // return Future.value(true);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return Future.error('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('Correo ya Existe');

        return Future.error('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }


  Future<void> ingresarEmail(dynamic email, dynamic pass) async {
    try {
      UserCredential usuario =
      await auth.signInWithEmailAndPassword(email: email, password: pass);
      _usuarior.value = usuario.user!.email;
      _uid.value = usuario.user!.uid;
      _name.value = usuario.user!.email;
      _photo.value = 'https://static.wikia.nocookie.net/shingeki-no-kyojin/images/2/2d/Levi_Ackerman_%28Anime%29.png/revision/latest?cb=20180816051528&path-prefix=es';


      //    _photo.value = usuario.user!.photoURL;
      print(usuario);
      await guardarusuario(_usuarior.value, pass);
      return Future.value(true);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        print('Correo no encontrado');
        return Future.error('user-not-found');
      } else if (e.code == 'wrong-password') {
        print('Password incorrecto');
        return Future.error('wrong-password');
      }
    }
  }



  Future<void> ingresarGoogle() async {
    // Trigger the authentication flow

    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
      // Obtain the auth details from the request

      final GoogleSignInAuthentication googleAuth =
      await googleUser!.authentication;

      // Create a new credential
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      // Once signed in, return the UserCredential
      UserCredential usuario =
        await FirebaseAuth.instance.signInWithCredential(credential);
      _usuarior.value = usuario.user!.email;
      _uid.value = usuario.user!.uid;

      _name.value = usuario.user!.displayName;
      _photo.value = usuario.user!.photoURL;

      return Future.value(true);
    } catch (e) {
      return Future.error('Error');
    }
  }

  Future<void> guardarusuario(datos, passw) async {
    Future<SharedPreferences> _localuser = SharedPreferences.getInstance();
    final SharedPreferences localuser = await _localuser;
    print("datos =>" + datos);
    localuser.setString('usuario', datos);
    localuser.setString('clave', passw);
    print(localuser.getString('usuario'));
  }

}
