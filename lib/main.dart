import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth1270_firebase/controller/controllerauth.dart';
import 'package:flutter_auth1270_firebase/controller/controllerfirestore.dart';
import 'package:flutter_auth1270_firebase/controller/controlrealtime.dart';
import 'package:flutter_auth1270_firebase/ui/app.dart';
import 'package:get/get.dart';

import 'controller/controllerchat.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Get.put(Controllerauth());
  Get.put(ControllerRealTime());
  Get.put(Controlchat());
  Get.put(ControllerFirestore());
  runApp(MyApp());
}
