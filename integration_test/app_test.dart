import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth1270_firebase/controller/controllerauth.dart';
import 'package:flutter_auth1270_firebase/ui/app.dart';
import 'package:flutter_auth1270_firebase/ui/pages/login.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  // setUp(() {});
  //
  // IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  // testWidgets('Simple Login Test', (WidgetTester tester) async {
  //   Get.put(Controllerauth());
  //   await tester.pumpWidget(MyApp());
  //
  //   await tester.pump();
  //
  //   expect(find.text('Testing demo'), findsOneWidget);
  //
  //   await tester.enterText(find.byKey(const Key('user')), 'Jeremy');
  //
  //   await tester.pump();
  //
  //   expect(find.text('Jeremy'), findsOneWidget);
  //
  // });

  testWidgets('MyWidget has a title and message', (WidgetTester tester) async {
    final addField = find.byKey(ValueKey("TextFieldName"));
    print('exists');
  });

  testWidgets('MyWidget has a title and message', (WidgetTester tester) async {
    await tester
        .pumpWidget(const MyWidget(title: 'Título', message: 'Mensaje'));
    // Create the Finders.
    final titleFinder = find.text('Título');
    final messageFinder = find.text('Mensaje');

    final pruebademoFinder = find.text('Testing Demo');

    // Use the findsOneWidget matcher provided by flutter_test to
    // verify that the Text widgets appear exactly once in the widget tree.
    expect(titleFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
    expect(pruebademoFinder, findsOneWidget);
  });

  testWidgets('MyWidget has a title and message', (WidgetTester tester) async {
    await tester.pumpWidget(const MyWidget(title: 'AT', message: 'AM'));
    // Create the Finders.
    final titleFinder = find.text('AT');
    final messageFinder = find.text('AM');

    // Use the findsOneWidget matcher provided by flutter_test to
    // verify that the Text widgets appear exactly once in the widget tree.
    expect(titleFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
  });

  testWidgets('MyWidget has a title and message', (WidgetTester tester) async {
    // Create the widget by telling the tester to build it.
    await tester.pumpWidget(const MyWidget(title: 'T', message: 'M'));

    // Create the Finders.
    final titleFinder = find.text('T');
    final messageFinder = find.text('M');

    // Use the findsOneWidget matcher provided by flutter_test to
    // verify that the Text widgets appear exactly once in the widget tree.
    expect(titleFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
  });
}

class MyWidget extends StatelessWidget {
  const MyWidget({
    Key? key,
    required this.title,
    required this.message,
  }) : super(key: key);

  final String title;
  final String message;

  @override
  Widget build(BuildContext context) {
    TextEditingController controller = TextEditingController();
    return MaterialApp(
        title: 'Flutter Demo',
        home: Scaffold(
            appBar: AppBar(
              title: Text(title),
            ),
            body: Center(
              child: Padding(
                padding: EdgeInsets.all(50.0),
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(message),
                      Text('Testing Demo'),
                      TextField(
                        key: const Key('TextFieldName'),
                        decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Your name',
                        ),
                        controller: controller,
                      ),
                    ]),
              ),
            )));
  } // Widget build
} // Class MyWidget
